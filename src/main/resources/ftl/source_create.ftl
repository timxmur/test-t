<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<#-- @ftlvariable name="form" type="NewsSourceForm" -->

<#import "/spring.ftl" as spring>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Добавить источник</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
<div class="row">
    <nav role="navigation">
        <ul>
    <#if !currentUser??>
        <li><a href="/login">Войти</a></li>
    </#if>
    <#if currentUser??>
        <li>
            <form action="/logout" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button type="submit">Выйти</button>
            </form>
        </li>
        <li><a href="/news">Новости</a></li>
        <#if currentUser.role == "ADMIN" >
            <li><a href="/sources">Источники новостей</a></li>
            <li><a href="/users">Пользователи</a></li>
        </#if>
    </#if>
        </ul>
    </nav>

    <h1>Добавить источник новостей</h1>

    <form role="form" name="form" action="/sources/create" method="post" class="form_create">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <div class="form_create-block">
            <div class="inp">
                <label for="type">Тип</label>
                <select name="type" id="type" required>
                    <option <#if form.type == 'RSS'>selected</#if>>RSS</option>
                    <option <#if form.type == 'HTML'>selected</#if>>HTML</option>
                </select>
            </div>
            <div class="inp">
                <label for="name">name</label>
                <input type="text" name="name" id="name" required autofocus/>
            </div>
            <div class="inp">
                <label for="uri">uri</label>
                <input type="text" name="uri" id="uri" required/>
            </div>
            <button type="submit">Save</button>
        </div>
        <div class="form_create-block2">
            <div class="inp">
                <label for="topElement">topElement</label>
                <input type="text" name="topElement" id="topElement"/>
            </div>
            <div class="inp">
                <label for="topElementType">topElementType</label>
                <input type="text" name="topElementType" id="topElementType"/>
            </div>
        </div>
        <div class="form_create-block2">
            <div class="inp">
                <label for="titleRule">titleRule</label>
                <input type="text" name="titleRule" id="titleRule"/>
            </div>
            <div class="inp">
                <label for="titleType">titleType</label>
                <input type="text" name="titleType" id="titleType"/>
            </div>
        </div>
        <div class="form_create-block2">
            <div class="inp">
                <label for="contentRule">contentRule</label>
                <input type="text" name="contentRule" id="contentRule"/>
            </div>
            <div class="inp">
                <label for="contentType">contentType</label>
                <input type="text" name="contentType" id="contentType"/>
            </div>
        </div>
        <div class="form_create-block2">
            <div class="inp">
                <label for="urlRule">urlRule</label>
                <input type="text" name="urlRule" id="urlRule"/>
            </div>
            <div class="inp">
                <label for="urlType">urlType</label>
                <input type="text" name="urlType" id="urlType"/>
            </div>
        </div>
        <div class="form_create-block2">
            <div class="inp">
                <label for="dateRule">dateRule</label>
                <input type="text" name="dateRule" id="dateRule"/>
            </div>
            <div class="inp">
                <label for="dateType">dateType</label>
                <input type="text" name="dateType" id="dateType"/>
            </div>
        </div>
    </form>

<@spring.bind "form" />
<#if spring.status.error>
<ul>
    <#list spring.status.errorMessages as error>
        <li>${error}</li>
    </#list>
</ul>
</#if>
</div>
</body>
</html>