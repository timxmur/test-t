<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<#-- @ftlvariable name="currentUser" type="CurrentUser" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Домашняя страница</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="row">
    <nav role="navigation">
        <ul>
    <#if !currentUser??>
        <li><a href="/login">Войти</a></li>
    </#if>
    <#if currentUser??>
        <li>
            <form action="/logout" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button type="submit">Выйти</button>
            </form>
        </li>
        <li><a href="/news">Новости</a></li>
        <#if currentUser.role == "ADMIN" >
            <li><a href="/sources">Источники новостей</a></li>
            <li><a href="/users">Пользователи</a></li>
        </#if>
    </#if>
        </ul>
    </nav>
</div>
</body>
</html>