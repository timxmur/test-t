<#-- @ftlvariable name="user" type="User" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Пользователь</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
<div class="row">
    <nav role="navigation">
        <ul>
    <#if !currentUser??>
        <li><a href="/login">Войти</a></li>
    </#if>
    <#if currentUser??>
        <li>
            <form action="/logout" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button type="submit">Выйти</button>
            </form>
        </li>
        <li><a href="/news">Новости</a></li>
        <#if currentUser.role == "ADMIN" >
            <li><a href="/sources">Источники новостей</a></li>
            <li><a href="/users">Пользователи</a></li>
        </#if>
    </#if>
        </ul>
    </nav>

    <h1>User details</h1>

    <p>E-mail: ${user.email}</p>

    <p>Имя: ${user.firstname} </p>

    <p>Фамилия: ${user.lastname} </p>

    <p>Role: ${user.role}</p>

</div>
</body>
</html>