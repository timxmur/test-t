package ru.timxmur.test.tochka.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.timxmur.test.tochka.domain.NewsSourceForm;
import ru.timxmur.test.tochka.service.ISourceService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class SourcesController {
    private final ISourceService sourceService;
    private static final Logger LOGGER = LoggerFactory.getLogger(SourcesController.class);


    @RequestMapping(value = "/sources", method = RequestMethod.GET)
    public ModelAndView getBooksPage() {
        return new ModelAndView("sources", "sources", sourceService.getAllSources());
    }

    @RequestMapping(value = "/sources/edit/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ModelAndView getSourceEditPage(@PathVariable Long id) {

        Map<String, Object> map= new HashMap<>();
        map.put("source",
                sourceService
                        .getById(id)
                        .orElseThrow(() -> new NoSuchElementException(String.format("Source=%s not found", id))));
        map.put("form", new NewsSourceForm());
        return new ModelAndView("source_edit",map);    }

    @RequestMapping(value = "/sources/create", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ModelAndView getSourceCreatePage() {
        return new ModelAndView("source_create", "form", new NewsSourceForm());
    }

    @RequestMapping(value = "/sources/delete/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public String deleteSource(@PathVariable Long id) {
        LOGGER.debug("Trying to delete source={}",id);
        sourceService.delete(id);
        return "redirect:/sources/";
    }

    @RequestMapping(value = "/sources/create", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public String handleSourceCreateForm(@Valid @ModelAttribute("form") NewsSourceForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "source_create";
        }
        try {
            LOGGER.debug("Trying to update source from form={}",form);

            sourceService.update(form, Optional.empty());
        } catch (DataIntegrityViolationException e) {
            LOGGER.error("Cannot update NewsSource: {}",e);
            return "source_create";
        }

        return "redirect:/sources/";
    }

    @RequestMapping(value = "/sources/edit/{id}", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public String handleSourceEditForm(@Valid @ModelAttribute("form") NewsSourceForm form, @PathVariable Long id, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "source_create";
        }
        try {
            LOGGER.debug("Trying to edit source={} with form{}", id, form);
            sourceService.update(form, Optional.of(id));
        } catch (DataIntegrityViolationException e) {
            LOGGER.error("Cannot edit NewsSource: {}",e);
            return "source_create";
        }

        return "redirect:/sources/";
    }

}
