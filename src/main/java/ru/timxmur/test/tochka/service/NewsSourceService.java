package ru.timxmur.test.tochka.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.timxmur.test.tochka.domain.*;
import ru.timxmur.test.tochka.repository.RuleRepository;
import ru.timxmur.test.tochka.repository.SourceRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NewsSourceService implements ISourceService {
    private final SourceRepository sourceRepository;
    private final RuleRepository ruleRepository;

    @Override
    public void delete(Long id) {
        sourceRepository.delete(id);
    }

    @Override
    public List<NewsSource> getAllSources() {
        return sourceRepository.findAll();
    }

    @Override
    public void update(NewsSourceForm form, Optional<Long> id) {
        NewsSource newsSource = new NewsSource();
        newsSource.setName(form.getName());
        newsSource.setUri(form.getUri());
        newsSource.setType((form.getType()));
        id.ifPresent(newsSource::setSourceId);

        Rule rule = new Rule();

        if (newsSource.getType().equals(NewsSourceTypeEnum.RSS)) {
            if (form.getTopElement().equals("")) rule.setTopElement("item");
            else rule.setTopElement(form.getTopElement());
            if (form.getContentRule().equals("")) rule.setContentRule("description");
            else rule.setContentRule(form.getContentRule());
            if (form.getUrlRule().equals("")) rule.setUrlRule("link");
            else rule.setUrlRule(form.getUrlRule());
            if (form.getTitleRule().equals("")) rule.setTitleRule("title");
            else rule.setTitleRule(form.getTitleRule());
            rule.setContentType(form.getContentType());
            rule.setTitleType(form.getTitleType());
            rule.setTopElementType(form.getTopElementType());
            rule.setUrlType(form.getUrlType());
        } else {
            rule.setContentRule(form.getContentRule());
            rule.setContentType(form.getContentType());
            rule.setTitleRule(form.getTitleRule());
            rule.setTitleType(form.getTitleType());
            rule.setTopElement(form.getTopElement());
            rule.setTopElementType(form.getTopElementType());
            rule.setUrlRule(form.getUrlRule());
            rule.setUrlType(form.getUrlType());
        }

        rule.setSource(newsSource);
        newsSource.setRule(rule);

        save(newsSource);
    }

    @Override
    public void save(NewsSource source) {
        sourceRepository.findOneBySourceId(source.getSourceId()).ifPresent(n -> {
            source.setSourceId(n.getSourceId());
            source.getRule().setRuleId(n.getRule().getRuleId());
        });
        sourceRepository.saveAndFlush(source);
    }

    @Override
    public Optional<NewsSource> getById(Long id) {
        return sourceRepository.findOneBySourceId(id);
    }
}
