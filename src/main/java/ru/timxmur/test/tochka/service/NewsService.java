package ru.timxmur.test.tochka.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.timxmur.test.tochka.domain.News;
import ru.timxmur.test.tochka.domain.NewsSource;
import ru.timxmur.test.tochka.repository.NewsRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NewsService implements INewsService {
    private final NewsRepository newsRepository;

    @Override
    public List<News> getAllNews() {
        return newsRepository.findAllByOrderByNewsIdDesc();
    }

    @Override
    public Optional<News> getById(Long id){
        return newsRepository.findOneByNewsId(id);
    }

    @Override
    public void save(News news){
        newsRepository.findOneByTitle(news.getTitle()).ifPresent(n -> news.setNewsId(n.getNewsId()));
        newsRepository.save(news);
    }

    @Override
    public void delete(Long id){
        newsRepository.delete(id);
    }

    @Override
    public List<News> getBySource(NewsSource source){
        return newsRepository.findOneBySource(source);
    }
}
