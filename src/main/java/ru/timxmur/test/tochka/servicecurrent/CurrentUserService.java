package ru.timxmur.test.tochka.servicecurrent;

import ru.timxmur.test.tochka.domain.CurrentUser;

public interface CurrentUserService {
    boolean canAccessUser(CurrentUser currentUser, Long userId);

}
